package org.beetlframework.fault;

/**
 * 授权异常（当权限无效时抛出）
 *
 * @author 草原狼
 * @since 1.0
 */
public class AuthzException extends RuntimeException {
    
	private static final long serialVersionUID = -5263921182370981833L;

	public AuthzException() {
        super();
    }

    public AuthzException(String message) {
        super(message);
    }

    public AuthzException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthzException(Throwable cause) {
        super(cause);
    }
}
