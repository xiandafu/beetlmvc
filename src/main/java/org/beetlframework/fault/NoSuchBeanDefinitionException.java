package org.beetlframework.fault;

public class NoSuchBeanDefinitionException extends RuntimeException{
	
	private static final long serialVersionUID = 5994470159467648486L;

	public NoSuchBeanDefinitionException() {
        super();
    }

    public NoSuchBeanDefinitionException(String message) {
        super(message);
    }

    public NoSuchBeanDefinitionException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoSuchBeanDefinitionException(Throwable cause) {
        super(cause);
    }
}
