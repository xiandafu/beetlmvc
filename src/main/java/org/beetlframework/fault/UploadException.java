package org.beetlframework.fault;

/**
 * 上传异常（当文件上传失败时抛出）
 *
 * @author 草原狼
 * @since 1.0
 */
public class UploadException extends RuntimeException {

   
	private static final long serialVersionUID = -9202341601178746445L;

	public UploadException() {
        super();
    }

    public UploadException(String message) {
        super(message);
    }

    public UploadException(String message, Throwable cause) {
        super(message, cause);
    }

    public UploadException(Throwable cause) {
        super(cause);
    }
}
