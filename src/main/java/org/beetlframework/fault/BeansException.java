package org.beetlframework.fault;

public class BeansException extends RuntimeException {

	private static final long serialVersionUID = 7552121924127620081L;
	public BeansException() {
        super();
    }

    public BeansException(String message) {
        super(message);
    }

    public BeansException(String message, Throwable cause) {
        super(message, cause);
    }

    public BeansException(Throwable cause) {
        super(cause);
    }
}
