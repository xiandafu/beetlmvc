package org.beetlframework.fault;

/**
 * 初始化错误
 *
 * @author 草原狼
 * @since 1.0
 */
public class InitializationError extends Error {

    
	private static final long serialVersionUID = -5220724217668280627L;

	public InitializationError() {
        super();
    }

    public InitializationError(String message) {
        super(message);
    }

    public InitializationError(String message, Throwable cause) {
        super(message, cause);
    }

    public InitializationError(Throwable cause) {
        super(cause);
    }
}
