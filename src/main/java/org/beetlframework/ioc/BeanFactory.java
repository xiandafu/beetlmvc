package org.beetlframework.ioc;

import org.beetlframework.fault.BeansException;
import org.beetlframework.fault.NoSuchBeanDefinitionException;

/**
 * Bean容器接口
 * @author 草原狼
 * @date 2016-10-6
 * 目前该接口无对应的实现类，打算在2.0版本进行实现
 */
public interface BeanFactory {
	/*
     * 四个不同形式的getBean方法，获取实例
     */
    Object getBean(String name) throws BeansException;
 
    <T> T getBean(String name, Class<T> requiredType) throws BeansException;
    //获取 Bean Map
    <T> T getBean(Class<T> requiredType) throws BeansException;
 
    Object getBean(String name, Object... args) throws BeansException;
    // 是否存在
    boolean containsBean(String name); 
    // 是否为单实例
    boolean isSingleton(String name) throws NoSuchBeanDefinitionException;
    // 是否为原型（多实例）
    boolean isPrototype(String name) throws NoSuchBeanDefinitionException;
    // 名称、类型是否匹配
    boolean isTypeMatch(String name, Class<?> targetType)
            throws NoSuchBeanDefinitionException;
    // 获取类型
    Class<?> getType(String name) throws NoSuchBeanDefinitionException; 
    //设置 Bean 实例
    public void setBean(Class<?> cls, Object obj);
}
