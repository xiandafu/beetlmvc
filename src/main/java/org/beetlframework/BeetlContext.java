package org.beetlframework;

import java.io.IOException;
import org.beetl.core.Configuration;
import org.beetl.core.GroupTemplate;
import org.beetl.core.resource.WebAppResourceLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * beetl环境
 * @author 草原狼
 * @date 2016-10-5
 */
public class BeetlContext {
	private static final Logger logger = LoggerFactory.getLogger(BeetlContext.class);
	private static GroupTemplate groupTemplate = null;
	
	public static GroupTemplate getGroupTemplate() {
		if(groupTemplate == null){
			logger.info("[beetl] 运行环境没有被初始化");
		}
		return groupTemplate;
	}

	public static void initCtx(String root){
		if (groupTemplate != null){		
			groupTemplate.close();
		}
		try{
			Configuration cfg = Configuration.defaultConfiguration();
			WebAppResourceLoader resourceLoader = new WebAppResourceLoader(root);
			groupTemplate = new GroupTemplate(resourceLoader, cfg);
		}catch(IOException e){
			throw new RuntimeException("初始 [beetl] 环境失败",e);
		}
	}
}
