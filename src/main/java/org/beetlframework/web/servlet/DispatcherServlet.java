package org.beetlframework.web.servlet;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.beetlframework.FrameworkConstant;
import org.beetlframework.InstanceFactory;

import org.beetlframework.mvc.Handler;
import org.beetlframework.mvc.HandlerExceptionResolver;
import org.beetlframework.mvc.HandlerInvoker;
import org.beetlframework.mvc.HandlerMapping;
import org.beetlframework.mvc.ServletActionContext;
import org.beetlframework.mvc.UploadHelper;
import org.beetlframework.util.WebUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 大江东去，浪淘尽。
 * 千古风流人物。
 * 故垒西边，人道是，三国周郎赤壁。
 * 乱石崩云，惊涛拍岸，卷起千堆雪。
 * 江山如画，一时多少豪杰!
 * ============================
 * 
 * Beetl MVC配置
 * @author 草原狼
 * @date 2016-10-6
 */
@WebServlet(urlPatterns = "/*", loadOnStartup = 0)
public class DispatcherServlet extends HttpServlet {	
	private static final long serialVersionUID = -2594174767874048587L;
	
	private static final Logger logger = LoggerFactory.getLogger(DispatcherServlet.class);

    private HandlerMapping handlerMapping = InstanceFactory.getHandlerMapping();
    private HandlerInvoker handlerInvoker = InstanceFactory.getHandlerInvoker();
    private HandlerExceptionResolver handlerExceptionResolver = InstanceFactory.getHandlerExceptionResolver();

    @Override
    public void init(ServletConfig config) throws ServletException {
        // 初始化相关配置
        ServletContext servletContext = config.getServletContext();
        UploadHelper.init(servletContext);
    }

    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 设置请求编码方式
        request.setCharacterEncoding(FrameworkConstant.ENCODING);
        // 获取当前请求相关数据
        String currentRequestMethod = getRequestMethod(request);
        String currentRequestPath = WebUtil.getRequestPath(request);
        logger.debug("[beetl] {}:{}", currentRequestMethod, currentRequestPath);
        // 将“/”请求重定向到首页
        if (currentRequestPath.equals("/")) {
            WebUtil.redirectRequest(FrameworkConstant.HOME_PAGE, request, response);
            return;
        }
        // 去掉当前请求路径末尾的“/”
        if (currentRequestPath.endsWith("/")) {
            currentRequestPath = currentRequestPath.substring(0, currentRequestPath.length() - 1);
        }
        // 获取 Handler
        Handler handler = handlerMapping.getHandler(currentRequestMethod, currentRequestPath);
        // 若未找到 Action，则跳转到 404 页面
        if (handler == null) {
            WebUtil.sendError(HttpServletResponse.SC_NOT_FOUND, "", response);
            return;
        }
        // 初始化 DataContext
        ServletActionContext.init(request, response);
        try {
            // 调用 Handler
            handlerInvoker.invokeHandler(request, response, handler);
        } catch (Exception e) {
            // 处理 Action 异常
            handlerExceptionResolver.resolveHandlerException(request, response, e);
        } finally {
            // 销毁 DataContext
            ServletActionContext.destroy();
        }
    }
    
    /**
     * 获取请求方法对应的注解
     * @param request
     * @return
     */
    private String getRequestMethod(HttpServletRequest request){
    	String method = request.getMethod();
    	if("POST".equals(method) || "GET".equals(method)){
    		return "Action";
    	}else{
    		return method;
    	}
    	
    }

}
