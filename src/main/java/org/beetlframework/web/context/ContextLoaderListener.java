package org.beetlframework.web.context;

import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebListener;

import org.beetlframework.BeetlContext;
import org.beetlframework.FrameworkConstant;
import org.beetlframework.ContainerLoader;
import org.beetlframework.plugin.Plugin;
import org.beetlframework.plugin.PluginHelper;
import org.beetlframework.plugin.WebPlugin;
import org.beetlframework.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 对Beetl容器进行实例化
 * @author 草原狼
 * @date 2016-10-6
 */
@WebListener
public class ContextLoaderListener implements ServletContextListener{
	private static final Logger logger = LoggerFactory.getLogger(ServletContextListener.class);
	/**
     * 当容器初始化时调用
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        // 获取 ServletContext
        ServletContext servletContext = sce.getServletContext();
        // 初始化容器加载类
        ContainerLoader.init();
        // 添加 Servlet 映射
        addServletMapping(servletContext);
        // 注册 WebPlugin
        registerWebPlugin(servletContext);
        // 加载Beetl
        loaderBeetl(servletContext);
    }

    private void loaderBeetl(ServletContext servletContext) {
    	String root = servletContext.getRealPath("");
    	BeetlContext.initCtx(root);
    	logger.info("[beetl] 运行环境初始化完成");
	}

	/**
     * 当容器销毁时调用
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        // 销毁插件
        destroyPlugin();
    }

    private void addServletMapping(ServletContext context) {
        // 用 DefaultServlet 映射所有静态资源
        registerDefaultServlet(context);
        // 用 JspServlet 映射所有 JSP 请求
        registerJspServlet(context);
        // 注册当前应用
        registerContextServlet(context);
    }

    private void registerContextServlet(ServletContext context) {
    	String ctx = context.getContextPath();
    	if(ctx.startsWith("/")){
    		ctx = ctx.substring(1);
    	}
    	ServletRegistration contextServlet = context.getServletRegistration(ctx);
    	contextServlet.addMapping("/*");
	}

	private void registerDefaultServlet(ServletContext context) {
        ServletRegistration defaultServlet = context.getServletRegistration("default");
        defaultServlet.addMapping("/index.html");
        defaultServlet.addMapping("/favicon.ico");
        String wwwPath = FrameworkConstant.WWW_PATH;
        if (StringUtil.isNotEmpty(wwwPath)) {
            defaultServlet.addMapping(wwwPath + "*");
        }
    }

    private void registerJspServlet(ServletContext context) {
    	
        ServletRegistration jspServlet = context.getServletRegistration("jsp");
        jspServlet.addMapping("/index.jsp");
        String jspPath = FrameworkConstant.JSP_PATH;
        if (StringUtil.isNotEmpty(jspPath)) {
            jspServlet.addMapping(jspPath + "*");
        }
    }

    private void registerWebPlugin(ServletContext servletContext) {
        List<Plugin> pluginList = PluginHelper.getPluginList();
        for (Plugin plugin : pluginList) {
            if (plugin instanceof WebPlugin) {
                WebPlugin webPlugin = (WebPlugin) plugin;
                webPlugin.register(servletContext);
            }
        }
    }

    private void destroyPlugin() {
        List<Plugin> pluginList = PluginHelper.getPluginList();
        for (Plugin plugin : pluginList) {
            plugin.destroy();
        }
    }


}
