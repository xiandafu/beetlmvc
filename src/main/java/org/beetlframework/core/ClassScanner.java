package org.beetlframework.core;

import java.lang.annotation.Annotation;
import java.util.List;

/**
 * 类扫描器
 *
 * @author 草原狼
 * @since 1.0
 */
public interface ClassScanner {

    /**
     * 获取指定包名中的所有类
     */
    List<Class<?>> getClassList(String packageName);

    /**
     * 获取指定包名中指定注解的相关类
     */
    List<Class<?>> getClassListByAnnotation(String packageName, Class<? extends Annotation> annotationClass);

    /**
     * 获取指定指定父类或接口(带包名路径)的相关类
     */
    List<Class<?>> getClassListBySuper(String packageName, Class<?> superClass);
    /**
     * 获取指定包名中指定后缀类名的相关类
     * @param packageName
     * @param suffix
     * @return
     */
    public List<Class<?>> getClassListByNameSuffix(String packageName, String suffix);
}
