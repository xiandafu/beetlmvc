package org.beetlframework.core;

import java.lang.annotation.Annotation;
import java.util.List;

import org.beetlframework.InstanceFactory;

/**
 * 根据条件获取相关类
 *
 * @author 草原狼
 * @since 1.0
 */
public class ClassHelper {

    /**
     * 获取基础包名
     */
    private static final String basePackage = ConfigHelper.getString("beetl.framework.app.base_package");

    /**
     * 获取 ClassScanner
     */
    private static final ClassScanner classScanner = InstanceFactory.getClassScanner();

    /**
     * 获取基础包名中的所有类
     */
    public static List<Class<?>> getClassList() {
        return classScanner.getClassList(basePackage);
    }

    /**
     * 获取基础包名中指定父类或接口的相关类
     */
    public static List<Class<?>> getClassListBySuper(Class<?> superClass) {
        return classScanner.getClassListBySuper(basePackage, superClass);
    }

    /**
     * 获取基础包名中指定注解的相关类
     */
    public static List<Class<?>> getClassListByAnnotation(Class<? extends Annotation> annotationClass) {
        return classScanner.getClassListByAnnotation(basePackage, annotationClass);
    }
    
    /**
     * 获取基础包名中指定后缀类名的相关类 
     */
    public static List<Class<?>> getClassListByNameSuffix(String suffix) {
        return classScanner.getClassListByNameSuffix(basePackage, suffix);
    }
}
