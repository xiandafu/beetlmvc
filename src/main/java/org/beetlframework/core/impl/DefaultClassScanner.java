package org.beetlframework.core.impl;

import java.lang.annotation.Annotation;
import java.util.List;

import org.beetlframework.core.ClassScanner;
import org.beetlframework.core.impl.support.AnnotationClassTemplate;
import org.beetlframework.core.impl.support.ClassTemplate;
import org.beetlframework.core.impl.support.SuffixClassTemplate;
import org.beetlframework.core.impl.support.SupperClassTemplate;

/**
 * 默认类扫描器
 *
 * @author 草原狼
 * @since 1.0
 */
public class DefaultClassScanner implements ClassScanner {
	/**
     * 获取指定包名中的所有类
     */
    @Override
    public List<Class<?>> getClassList(String packageName) {
        return new ClassTemplate(packageName) {
            @Override
            public boolean checkAddClass(Class<?> cls) {
                String className = cls.getName();
                String pkgName = className.substring(0, className.lastIndexOf("."));
                return pkgName.startsWith(packageName);
            }
        }.getClassList();
    }
    
    /**
     * 获取指定包名中指定注解的相关类
     */
    @Override
    public List<Class<?>> getClassListByAnnotation(String packageName, Class<? extends Annotation> annotationClass) {
        return new AnnotationClassTemplate(packageName, annotationClass) {
            @Override
            public boolean checkAddClass(Class<?> cls) {
                return cls.isAnnotationPresent(annotationClass);
            }
        }.getClassList();
    }

    /**
     * 获取指定包名中指定父类或接口的相关类
     */
    @Override
    public List<Class<?>> getClassListBySuper(String packageName, Class<?> superClass) {
        return new SupperClassTemplate(packageName, superClass) {
            @Override
            public boolean checkAddClass(Class<?> cls) {
                return superClass.isAssignableFrom(cls) && !superClass.equals(cls);
            }
        }.getClassList();
    }

    /**
     * 获取指定包名中指定后缀类名的相关类
     * @param packageName
     * @param suffix
     * @return
     */
	@Override
	public List<Class<?>> getClassListByNameSuffix(String packageName, String suffix) {		
		return new SuffixClassTemplate(packageName,suffix){
			@Override
			public boolean checkAddClass(Class<?> cls) {
				return cls.getName().endsWith(this.suffix);				
			}
		}.getClassList();
	}
}
