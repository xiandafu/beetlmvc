package org.beetlframework.core.impl.support;

/**
 * 用于获取子类的模板类
 *
 * @author 草原狼
 * @since 1.0
 */
public abstract class SupperClassTemplate extends ClassTemplate {

    protected final Class<?> superClass;

    protected SupperClassTemplate(String packageName, Class<?> superClass) {
        super(packageName);
        this.superClass = superClass;
    }
}
