package org.beetlframework.core.impl.support;



/**
 * 获取满足指定后缀名的类模板类
 *
 * @author 草原狼
 * @since 1.0
 */
public abstract class SuffixClassTemplate extends ClassTemplate {
	protected final String suffix;
	
	protected SuffixClassTemplate(String packageName,String suffix) {
		super(packageName);
		this.suffix = suffix;
	}

}
