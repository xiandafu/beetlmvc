package org.beetlframework.plugin;

/**
 * 插件接口
 *
 * @author 草原狼
 * @since 1.0
 */
public interface Plugin {

    /**
     * 初始化插件
     */
    void init();

    /**
     * 销毁插件
     */
    void destroy();
}
