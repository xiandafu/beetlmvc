package org.beetlframework.plugin;

import java.util.List;

import org.beetlframework.aop.proxy.Proxy;

/**
 * 插件代理
 *
 * @author 草原狼
 * @since 1.0
 */
public abstract class PluginProxy implements Proxy {

    public abstract List<Class<?>> getTargetClassList();
}

