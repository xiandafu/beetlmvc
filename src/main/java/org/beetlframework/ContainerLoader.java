package org.beetlframework;

import org.beetlframework.aop.AopHelper;
import org.beetlframework.ioc.BeanContext;
import org.beetlframework.ioc.IocHelper;
import org.beetlframework.mvc.ActionMapping;
import org.beetlframework.plugin.PluginHelper;
import org.beetlframework.util.ClassLoaderUtil;

/**
 * 容器加载类
 *
 * @author 草原狼
 * @since 1.0
 */
public final class ContainerLoader {

    public static void init() {
        // 定义需要加载的 Helper 类
        Class<?>[] classList = {
        	/*	
            DatabaseHelper.class,
            EntityHelper.class,           
            */
            ActionMapping.class,
            BeanContext.class,
            AopHelper.class,
            IocHelper.class,
            PluginHelper.class,
            
        };
        // 按照顺序加载类
        for (Class<?> cls : classList) {
            ClassLoaderUtil.loadClass(cls.getName());
        }
    }
}
