package org.beetlframework;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.beetlframework.core.ClassScanner;
import org.beetlframework.core.ConfigHelper;
import org.beetlframework.core.impl.DefaultClassScanner;
import org.beetlframework.mvc.HandlerExceptionResolver;
import org.beetlframework.mvc.HandlerInvoker;
import org.beetlframework.mvc.HandlerMapping;
import org.beetlframework.mvc.ViewResolver;
import org.beetlframework.mvc.impl.BeetlHandlerInvoker;
import org.beetlframework.mvc.impl.BeetlViewResolver;
import org.beetlframework.mvc.impl.DefaultHandlerExceptionResolver;
import org.beetlframework.mvc.impl.DefaultHandlerMapping;
import org.beetlframework.util.ObjectUtil;
import org.beetlframework.util.StringUtil;

/**
 * 实例工厂
 *
 * @author 草原狼
 * @since 1.0
 */
public class InstanceFactory {

    /**
     * 用于缓存对应的实例
     */
    private static final Map<String, Object> cache = new ConcurrentHashMap<String, Object>();

    /**
     * ClassScanner
     */
    private static final String CLASS_SCANNER = "beetl.framework.custom.class_scanner";

    /**
     * DataSourceFactory
     */
    //private static final String DS_FACTORY = "beetl.framework.custom.ds_factory";

    /**
     * DataAccessor
     */
    //private static final String DATA_ACCESSOR = "beetl.framework.custom.data_accessor";

    /**
     * HandlerMapping
     */
    private static final String HANDLER_MAPPING = "beetl.framework.custom.handler_mapping";

    /**
     * HandlerInvoker
     */
    private static final String HANDLER_INVOKER = "beetl.framework.custom.handler_invoker";

    /**
     * HandlerExceptionResolver
     */
    private static final String HANDLER_EXCEPTION_RESOLVER = "beetl.framework.custom.handler_exception_resolver";

    /**
     * ViewResolver
     */
    private static final String VIEW_RESOLVER = "beetl.framework.custom.view_resolver";

    /**
     * 获取 ClassScanner
     */
    public static ClassScanner getClassScanner() {
        return getInstance(CLASS_SCANNER, DefaultClassScanner.class);
    }

    /**
     * 获取 DataSourceFactory
     
    public static DataSourceFactory getDataSourceFactory() {
        return getInstance(DS_FACTORY, DefaultDataSourceFactory.class);
    }*/

    /**
     * 获取 DataAccessor
     
    public static DataAccessor getDataAccessor() {
        return getInstance(DATA_ACCESSOR, DefaultDataAccessor.class);
    }*/

    /**
     * 获取 HandlerMapping
     */
    public static HandlerMapping getHandlerMapping() {
        return getInstance(HANDLER_MAPPING, DefaultHandlerMapping.class);
    }

    /**
     * 获取 HandlerInvoker
     */
    public static HandlerInvoker getHandlerInvoker() {
        //return getInstance(HANDLER_INVOKER, DefaultHandlerInvoker.class);
        return getInstance(HANDLER_INVOKER, BeetlHandlerInvoker.class);
    }

    /**
     * 获取 HandlerExceptionResolver
     */
    public static HandlerExceptionResolver getHandlerExceptionResolver() {
        return getInstance(HANDLER_EXCEPTION_RESOLVER, DefaultHandlerExceptionResolver.class);
    }

    /**
     * 获取 ViewResolver
     */
    public static ViewResolver getViewResolver() {
        //return getInstance(VIEW_RESOLVER, DefaultViewResolver.class);
        return getInstance(VIEW_RESOLVER, BeetlViewResolver.class);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getInstance(String cacheKey, Class<T> defaultImplClass) {
        // 若缓存中存在对应的实例，则返回该实例
        if (cache.containsKey(cacheKey)) {
            return (T) cache.get(cacheKey);
        }
        // 从配置文件中获取相应的接口实现类配置
        String implClassName = ConfigHelper.getString(cacheKey);
        // 若实现类配置不存在，则使用默认实现类
        if (StringUtil.isEmpty(implClassName)) {
            implClassName = defaultImplClass.getName();
        }
        // 通过反射创建该实现类对应的实例
        T instance = ObjectUtil.newInstance(implClassName);
        // 若该实例不为空，则将其放入缓存
        if (instance != null) {
            cache.put(cacheKey, instance);
        }
        // 返回该实例
        return instance;
    }
}
