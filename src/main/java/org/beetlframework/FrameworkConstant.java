package org.beetlframework;

import org.beetlframework.core.ConfigHelper;

public interface FrameworkConstant {

    String ENCODING = ConfigHelper.getString("beetl.framework.character.encoding", "UTF-8");

    String CONFIG_PROPS = "beetl-mvc.properties";
    String SQL_PROPS = "beetl-dao-sql.properties";

    String PLUGIN_PACKAGE = "org.beetl.plugin";
    
    String JSP_PATH = ConfigHelper.getString("beetl.framework.app.jsp_path", "/WEB-INF/jsp/");
    String WWW_PATH = ConfigHelper.getString("beetl.framework.app.www_path", "/www/");
    String HOME_PAGE = ConfigHelper.getString("beetl.framework.app.home_page", "/index.html");
    int UPLOAD_LIMIT = ConfigHelper.getInt("beetl.framework.app.upload_limit", 10);
    //配置开发模式,默认为生产模式
    boolean DEV_MODE = ConfigHelper.getBoolean("beetl.framework.devmode", false);
}
