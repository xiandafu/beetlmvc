TODO
======================================================
1)支持 Controller后缀和映射关系检查--
2)支持监听器和请求入口类配置 --
3)代码梳理（命名规范化） ，包归并
4)集成beetl-mvc后，开始做dev分支
5)上传 maven仓库
6)获取 maven仓库下的beetlmvc来做demo
7)编写文档，公布API
8)支持拦截器技术
9)处理好插件技术
10)支持热插拔技术
11)action 映射冲突问题
备忘录
======================================================
beetl-mvc.properties 用于beelt和mvc框架进行整合的配置文件
用配置字符编码，默认UTF-8
beetl.framework.character.encoding=UTF-8
自定义请求映射器类
beetl.framework.custom.handler_mapping
自定义异常试图解析器类
beetl.framework.custom.handler_exception_resolver
自定义试图解析器类
beetl.framework.custom.view_resolver

配置开发模式,模式为生产模式
beetl.framework.devmode=true
-----------------------------------------------------
关于web.xml配置，其中web容器支持servlet3.0以上版本无需以下配置

   <display-name>beetlmvc Home Web</display-name>	
	<listener>    	<listener-class>org.beetlframework.mvc.ContainerListener</listener-class>
  	</listener>
	<servlet>
		<servlet-name>smart-bootstrap</servlet-name>
		<servlet-class>org.beetlframework.mvc.DispatcherServlet</servlet-class>
	</servlet>
	<servlet-mapping>
	    <servlet-name>smart-bootstrap</servlet-name>
	    <url-pattern>/*</url-pattern>
  </servlet-mapping>

其中smart-bootstrap代表应用名
